<?php
/**
 * The template for displaying archive pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package WordPress
 * @subpackage Twenty_Twenty_One
 * @since Twenty Twenty-One 1.0
 */

get_header();


$description = get_the_archive_description();
?>

<?php if ( have_posts() ) : ?>

	<header class="wp-block-cover alignfull has-transparent-background-color has-background-dim wave-background no-top-wave archive-header">
		<div class="wp-block-cover__inner-container">

			<h1 class="has-text-align-center has-white-color has-text-color">
				<strong>Le blog</strong>
			</h1>
			<?php $terms = get_terms( array('taxonomy' => 'category',) );?>
			<nav id="nav-category" class='alignwide'>
				<ul>
				<?php foreach ( $terms as $term ) :?>
					<li style="background-color:<?php the_field('color', $term);?>">
						<a href="<?php echo get_term_link($term);?>">
							<?php echo '<h2>'.$term->name . '</h2>';?>
						</a> 
					</li>
				<?php endforeach;?>
				<?php wp_reset_postdata(); ?>
				</ul>
			</nav>
			<!-- <div class="wp-block-spacer" style="height:250px"></div> -->
<!-- 			<?php the_archive_title( '<h1 class="page-title">', '</h1>' ); ?>
		<?php if ( $description ) : ?>
			<div class="archive-description"><?php echo wp_kses_post( wpautop( $description ) ); ?></div>
		<?php endif; ?> -->
		</div>
		
	</header><!-- .page-header -->
		<div class="blog-grid alignwide">
			<?php while ( have_posts() ) : ?>
				<?php the_post(); ?>
				<?php get_template_part( 'template-parts/content/content', get_theme_mod( 'display_excerpt_or_full_post', 'excerpt' ) ); ?>
			<?php endwhile; ?>
		</div>


	<nav class="alignwide blog-pagination">
		<div class="alignleft"><?php previous_posts_link('&laquo; Page précédente') ?></div>
		<div class="alignright"><?php next_posts_link('Page suivante &raquo;','') ?></div>
	</nav>

<?php else : ?>
	<?php get_template_part( 'template-parts/content/content-none' ); ?>
<?php endif; ?>

<?php get_footer(); ?>
