document.addEventListener('DOMContentLoaded', function() {
    var els = document.getElementsByClassName("wpcf7-form");
    Array.prototype.forEach.call(els, function(el) {
        el.removeAttribute("novalidate");
    });
});

if (window.innerWidth < 800){
    var mobile = document.getElementById("slideshow-container");

    var btnPrev = document.createElement('a');
    btnPrev.classList.add("prev");
    btnPrev.setAttribute('onclick','plusSlides(-1)');
    btnPrev.textContent = "❮";
    mobile.appendChild(btnPrev);

    var btnNext = document.createElement('a');
    btnNext.classList.add("next");
    btnNext.setAttribute('onclick', 'plusSlides(1)');
    btnNext.textContent = "❯";
    mobile.appendChild(btnNext);

    var slideIndex = 1;
    showSlides(slideIndex);
}

function plusSlides(n) {
    showSlides(slideIndex += n);
}

function currentSlide(n) 
{
    showSlides(slideIndex = n);
}

function showSlides(n) {
    var i;
    var slides = document.getElementsByClassName("mySlides");

    if (n > slides.length) {
        slideIndex = 1
    }    

    if (n < 1) {
        slideIndex = slides.length
    }

    for (i = 0; i < slides.length; i++) {
            slides[i].style.display = "none";  
    }

    slides[slideIndex-1].style.display = "initial";
}