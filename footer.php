<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package WordPress
 * @subpackage Twenty_Twenty_One
 * @since Twenty Twenty-One 1.0
 */

?>
			</main><!-- #main -->
		</div><!-- #primary -->
	</div><!-- #content -->

	
	<?php if ( is_active_sidebar( 'pre-footer' ) ) : ?>

	<div class="pre-footer">
		<?php dynamic_sidebar( 'pre-footer' ); ?>
	</div><!-- .widget-area -->

	<?php endif; ?>
	<footer id="colophon" class="site-footer" role="contentinfo">

	
		<div class="site-info">
			<?php if ( has_nav_menu( 'footer' ) ) : ?>
			<nav aria-label="<?php esc_attr_e( 'Secondary menu', 'twentytwentyone' ); ?>" class="footer-navigation">
				<p> ©2021 Greenmetrics • <?php esc_html_e('All rights reserved','twentytwentyone'); ?> </p>
				<ul class="footer-navigation-wrapper">
					<?php
					wp_nav_menu(
						array(
							'theme_location' => 'footer',
							'items_wrap'     => '%3$s',
							'container'      => false,
							'depth'          => 1,
/* 							'link_before'    => '<span>',
							'link_after'     => '</span>', */
							'fallback_cb'    => false,
						)
					);
					?>
				</ul><!-- .footer-navigation-wrapper -->
			</nav><!-- .footer-navigation -->
			<?php endif; ?>

			<?php if ( is_active_sidebar( 'sidebar-footer' ) ) : ?>
				<aside class="">
					<?php dynamic_sidebar( 'sidebar-footer' ); ?>
				</aside><!-- .widget-area -->
			<?php endif; ?>


		</div><!-- .site-info -->
	</footer><!-- #colophon -->

</div><!-- #page -->

<?php wp_footer(); ?>

</body>
</html>
