<?php
/*require 'vendor/autoload.php';
use \Mailjet\Resources;*/
/**
 * Functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package WordPress
 * @subpackage Twenty_Twenty_One
 * @since Twenty Twenty-One 1.0
 */
if (!defined('WP_POST_REVISIONS')) define('WP_POST_REVISIONS', 3);
if (!defined('AUTOSAVE_INTERVAL')) define('AUTOSAVE_INTERVAL', 600);
// Supprime la taxo Tags des Articles
function ihag_custom()
{
	register_taxonomy('post_tag', array("category"));
	remove_post_type_support('page', 'thumbnail');
}
add_action('init', 'ihag_custom');

add_filter('wp_revisions_to_keep', 'filter_function_name', 10, 2);
function filter_function_name($num, $post)
{
	return 3;
}

function rjs_lwp_contactform_css_js()
{
	global $post;
	if (is_a($post, 'WP_Post') && has_shortcode($post->post_content, 'contact-form-7')) {
		wp_enqueue_script('contact-form-7');
		wp_enqueue_style('contact-form-7');
	} else {
		wp_dequeue_script('contact-form-7');
		wp_dequeue_style('contact-form-7');
	}
}
add_action('wp_enqueue_scripts', 'rjs_lwp_contactform_css_js');

add_filter('wpseo_json_ld_output', '__return_false');

add_action('customize_register', 'jt_customize_register');
function jt_customize_register($wp_customize)
{

	$wp_customize->remove_control('custom_css');
}

if (!function_exists('ihag_setup')) {
	/**
	 * Sets up theme defaults and registers support for various WordPress features.
	 *
	 * Note that this function is hooked into the after_setup_theme hook, which
	 * runs before the init hook. The init hook is too late for some features, such
	 * as indicating support for post thumbnails.
	 *
	 * @since Twenty Twenty-One 1.0
	 *
	 * @return void
	 */
	function ihag_setup()
	{
		/*
		 * Make theme available for translation.
		 * Translations can be filed in the /languages/ directory.
		 * If you're building a theme based on Twenty Twenty-One, use a find and replace
		 * to change 'twentytwentyone' to the name of your theme in all the template files.
		 */
		load_theme_textdomain('ihag', get_theme_file_path() . '/languages');

		// Add default posts and comments RSS feed links to head.
		//add_theme_support( 'automatic-feed-links' );

		/*
		 * Let WordPress manage the document title.
		 * This theme does not use a hard-coded <title> tag in the document head,
		 * WordPress will provide it for us.
		 */
		//add_theme_support( 'title-tag' );

		/**
		 * Add post-formats support.
		 */
		/*add_theme_support(
			'post-formats',
			array(
				'small_width',
				'aside',
				'gallery',
				'image',
				'quote',
				'status',
				'video',
				'audio',
				'chat',
			)
		);*/

		/*
		 * Enable support for Post Thumbnails on posts and pages.
		 *
		 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
		 */
		add_theme_support('post-thumbnails');
		set_post_thumbnail_size(1568, 9999);

		add_image_size("150-150", 100, 100);
		add_image_size("350-350", 350, 350);
		add_image_size("450-450", 450, 450);
		add_image_size("650-650", 650, 650);
		add_filter('image_size_names_choose', 'ihag_custom_sizes');


		function ihag_custom_sizes($sizes)
		{
			return array_merge($sizes, array(
				'150-150' => __('Petite sans rognage'),
				'350-350' => __('Contact sans rognage'),
				'450-450' => __('Moyen sans rognage'),
				'650-650' => __('Grande sans rognage')
			));
		}
		register_nav_menus(
			array(
				'primary' => esc_html__('Menu Header', 'twentytwentyone'),
				'footer'  => __('Menu Footer', 'twentytwentyone'),
				//'secondary'  => __( 'Menu Header 2', 'twentytwentyone' ),
			)
		);

		/*
		 * Switch default core markup for search form, comment form, and comments
		 * to output valid HTML5.
		 */
		add_theme_support(
			'html5',
			array(
				'comment-form',
				'comment-list',
				'gallery',
				'caption',
				'style',
				'script',
				'navigation-widgets',
			)
		);

		/*
		 * Add support for core custom logo.
		 *
		 * @link https://codex.wordpress.org/Theme_Logo
		 */
		$logo_width  = 220;
		$logo_height = 43;

		add_theme_support(
			'custom-logo',
			array(
				'height'               => $logo_height,
				'width'                => $logo_width,
				'flex-width'           => true,
				'flex-height'          => true,
				'unlink-homepage-logo' => true,
			)
		);

		// Add theme support for selective refresh for widgets.
		add_theme_support('customize-selective-refresh-widgets');

		// Add support for Block Styles.
		add_theme_support('wp-block-styles');

		// Add support for full and wide align images.
		add_theme_support('align-wide');

		// Add support for editor styles.
		add_theme_support('editor-styles');
		//$background_color = get_theme_mod( 'background_color', 'FFFFFF' );

		/*if ( 127 > ihag_Custom_Colors::get_relative_luminance_from_hex( $background_color ) ) {
			add_theme_support( 'dark-editor-style' );
		}*/

		$editor_stylesheet_path = './assets/css/style-editor.css';

		// Enqueue editor styles.
		add_editor_style($editor_stylesheet_path);

		// Add custom editor font sizes.
		add_theme_support(
			'editor-font-sizes',
			array(
				array(
					'name'      => esc_html__('Extra small', 'twentytwentyone'),
					'shortName' => esc_html_x('XS', 'Font size', 'twentytwentyone'),
					'size'      => 12,
					'slug'      => 'extra-small',
				),
				array(
					'name'      => esc_html__('Small', 'twentytwentyone'),
					'shortName' => esc_html_x('S', 'Font size', 'twentytwentyone'),
					'size'      => 14,
					'slug'      => 'small',
				),
				array(
					'name'      => esc_html__('Normal', 'twentytwentyone'),
					'shortName' => esc_html_x('M', 'Font size', 'twentytwentyone'),
					'size'      => 16,
					'slug'      => 'normal',
				),
				array(
					'name'      => esc_html__('Large', 'twentytwentyone'),
					'shortName' => esc_html_x('L', 'Font size', 'twentytwentyone'),
					'size'      => 20,
					'slug'      => 'large',
				),
				array(
					'name'      => esc_html__('Extra large', 'twentytwentyone'),
					'shortName' => esc_html_x('XL', 'Font size', 'twentytwentyone'),
					'size'      => 26,
					'slug'      => 'extra-large',
				),
				array(
					'name'      => esc_html__('Huge', 'twentytwentyone'),
					'shortName' => esc_html_x('XXL', 'Font size', 'twentytwentyone'),
					'size'      => 48,
					'slug'      => 'huge',
				),
				array(
					'name'      => esc_html__('Gigantic', 'twentytwentyone'),
					'shortName' => esc_html_x('XXXL', 'Font size', 'twentytwentyone'),
					'size'      => 96,
					'slug'      => 'gigantic',
				),
			)
		);

		// Custom background color.
		/*add_theme_support(
			'custom-background',
			array(
				'default-color' => 'FFFFFF',
			)
		);*/

		// Editor color palette.
		$black     = '#000000';
		$dark_gray = '#8d8d8d';
		$gray      = '#6f6f6f';
		$light_gray = '#f9f9f9';
		$green     = '#0cf1d0';
		$blue      = '#2b81f8';
		//$purple    = '#D1D1E4';
		$red       = '#ff5965';
		//$orange    = '#E4DAD1';
		$blueishgray    = '#222834';
		$transparent	= 'transparent';
		$white     = '#FFFFFF';
		$gray222	= '#222';

		add_theme_support(
			'editor-color-palette',
			array(
				array(
					'name'  => esc_html__('Gray222', 'twentytwentyone'),
					'slug'  => 'gray222',
					'color' => $gray222,
				),
				array(
					'name'  => esc_html__('Black', 'twentytwentyone'),
					'slug'  => 'black',
					'color' => $black,
				),
				array(
					'name'  => esc_html__('Dark gray', 'twentytwentyone'),
					'slug'  => 'dark-gray',
					'color' => $dark_gray,
				),
				array(
					'name'  => esc_html__('Gray', 'twentytwentyone'),
					'slug'  => 'gray',
					'color' => $gray,
				),
				array(
					'name'  => esc_html__('Light gray', 'twentytwentyone'),
					'slug'  => 'light-gray',
					'color' => $light_gray,
				),
				array(
					'name'  => esc_html__('Green', 'twentytwentyone'),
					'slug'  => 'green',
					'color' => $green,
				),
				array(
					'name'  => esc_html__('Blue', 'twentytwentyone'),
					'slug'  => 'blue',
					'color' => $blue,
				),
				/*array(
					'name'  => esc_html__( 'Purple', 'twentytwentyone' ),
					'slug'  => 'purple',
					'color' => $purple,
				),*/
				array(
					'name'  => esc_html__('Red', 'twentytwentyone'),
					'slug'  => 'red',
					'color' => $red,
				),
				/*array(
					'name'  => esc_html__( 'Orange', 'twentytwentyone' ),
					'slug'  => 'orange',
					'color' => $orange,
				),*/
				array(
					'name'  => esc_html__('Blueishgray', 'twentytwentyone'),
					'slug'  => 'blueishgray',
					'color' => $blueishgray,
				),
				array(
					'name'  => esc_html__('White', 'twentytwentyone'),
					'slug'  => 'white',
					'color' => $white,
				),
				array(
					'name'  => esc_html__('Transparent', 'twentytwentyone'),
					'slug'  => 'transparent',
					'color' => $transparent,
				),
			)
		);

		/*add_theme_support(
			'editor-gradient-presets',
			array(
				array(
					'name'     => esc_html__( 'Purple to yellow', 'twentytwentyone' ),
					'gradient' => 'linear-gradient(160deg, ' . $purple . ' 0%, ' . $yellow . ' 100%)',
					'slug'     => 'purple-to-yellow',
				),
				array(
					'name'     => esc_html__( 'Yellow to purple', 'twentytwentyone' ),
					'gradient' => 'linear-gradient(160deg, ' . $yellow . ' 0%, ' . $purple . ' 100%)',
					'slug'     => 'yellow-to-purple',
				),
				array(
					'name'     => esc_html__( 'Green to yellow', 'twentytwentyone' ),
					'gradient' => 'linear-gradient(160deg, ' . $green . ' 0%, ' . $yellow . ' 100%)',
					'slug'     => 'green-to-yellow',
				),
				array(
					'name'     => esc_html__( 'Yellow to green', 'twentytwentyone' ),
					'gradient' => 'linear-gradient(160deg, ' . $yellow . ' 0%, ' . $green . ' 100%)',
					'slug'     => 'yellow-to-green',
				),
				array(
					'name'     => esc_html__( 'Red to yellow', 'twentytwentyone' ),
					'gradient' => 'linear-gradient(160deg, ' . $red . ' 0%, ' . $yellow . ' 100%)',
					'slug'     => 'red-to-yellow',
				),
				array(
					'name'     => esc_html__( 'Yellow to red', 'twentytwentyone' ),
					'gradient' => 'linear-gradient(160deg, ' . $yellow . ' 0%, ' . $red . ' 100%)',
					'slug'     => 'yellow-to-red',
				),
				array(
					'name'     => esc_html__( 'Purple to red', 'twentytwentyone' ),
					'gradient' => 'linear-gradient(160deg, ' . $purple . ' 0%, ' . $red . ' 100%)',
					'slug'     => 'purple-to-red',
				),
				array(
					'name'     => esc_html__( 'Red to purple', 'twentytwentyone' ),
					'gradient' => 'linear-gradient(160deg, ' . $red . ' 0%, ' . $purple . ' 100%)',
					'slug'     => 'red-to-purple',
				),
			)
		);*/

		/*
		* Adds starter content to highlight the theme on fresh sites.
		* This is done conditionally to avoid loading the starter content on every
		* page load, as it is a one-off operation only needed once in the customizer.
		*/
		if (is_customize_preview()) {
			require get_theme_file_path() . '/inc/starter-content.php';
			add_theme_support('starter-content', ihag_get_starter_content());
		}

		// Add support for responsive embedded content.
		add_theme_support('responsive-embeds');

		// Add support for custom line height controls.
		add_theme_support('custom-line-height');

		// Add support for experimental link color control.
		add_theme_support('experimental-link-color');

		// Add support for experimental cover block spacing.
		add_theme_support('custom-spacing');

		// Add support for custom units.
		// This was removed in WordPress 5.6 but is still required to properly support WP 5.5.
		add_theme_support('custom-units');
	}
}
add_action('after_setup_theme', 'ihag_setup');

/**
 * Register widget area.
 *
 * @since Twenty Twenty-One 1.0
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 *
 * @return void
 */
function ihag_widgets_init()
{

	register_sidebar(
		array(
			'name'          => esc_html__('Footer', 'twentytwentyone'),
			'id'            => 'sidebar-footer',
			'description'   => esc_html__('Add widgets here to appear in your footer.', 'twentytwentyone'),
			'before_widget' => '<section id="%1$s" class="widget %2$s">',
			'after_widget'  => '</section>',
			'before_title'  => '<h2 class="widget-title">',
			'after_title'   => '</h2>',
		)
	);
	register_sidebar(
		array(
			'name'          => esc_html__('Pré-footer', 'twentytwentyone'),
			'id'            => 'pre-footer',
			'description'   => esc_html__('Add widgets here to appear in your pre-footer.', 'twentytwentyone'),
			'before_widget' => '<section id="%1$s" class="widget %2$s">',
			'after_widget'  => '</section>',
			'before_title'  => '<h2 class="widget-title">',
			'after_title'   => '</h2>',
		)
	);
	register_sidebar(
		array(
			'name'          => esc_html__('Header', 'twentytwentyone'),
			'id'            => 'sidebar-header',
			'description'   => esc_html__('Add widgets here to appear in your header.', 'twentytwentyone'),
			'before_widget' => '<section id="%1$s" class="widget %2$s">',
			'after_widget'  => '</section>',
			/*'before_title'  => '<h2 class="widget-title">',
			'after_title'   => '</h2>',*/
		)
	);
}
add_action('widgets_init', 'ihag_widgets_init');

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @since Twenty Twenty-One 1.0
 *
 * @global int $content_width Content width.
 *
 * @return void
 */
function ihag_content_width()
{
	// This variable is intended to be overruled from themes.
	// Open WPCS issue: {@link https://github.com/WordPress-Coding-Standards/WordPress-Coding-Standards/issues/1043}.
	// phpcs:ignore WordPress.NamingConventions.PrefixAllGlobals.NonPrefixedVariableFound
	$GLOBALS['content_width'] = apply_filters('ihag_content_width', 750);
}
add_action('after_setup_theme', 'ihag_content_width', 0);

/**
 * Enqueue scripts and styles.
 *
 * @since Twenty Twenty-One 1.0
 *
 * @return void
 */
function ihag_scripts()
{
	// Note, the is_IE global variable is defined by WordPress and is used
	// to detect if the current browser is internet explorer.
	/*global $is_IE, $wp_scripts;
	if ( $is_IE ) {
		// If IE 11 or below, use a flattened stylesheet with static values replacing CSS Variables.
		wp_enqueue_style( 'twenty-twenty-one-style', get_template_directory() . '/assets/css/ie.css', array(), wp_get_theme()->get( 'Version' ) );
	} else {*/
	// If not IE, use the standard stylesheet.
	wp_enqueue_style('twenty-twenty-one-style', get_stylesheet_directory_uri() . '/style.css', array(), wp_get_theme()->get('Version'));
	//}

	// RTL styles.
	//wp_style_add_data( 'twenty-twenty-one-style', 'rtl', 'replace' );

	// Print styles.
	wp_enqueue_style('twenty-twenty-one-print-style', get_stylesheet_directory_uri() . '/assets/css/print.css', array(), wp_get_theme()->get('Version'), 'print');

	// Threaded comment reply styles.
	if (is_singular() && comments_open() && get_option('thread_comments')) {
		wp_enqueue_script('comment-reply');
	}

	// Register the IE11 polyfill file.
	/*wp_register_script(
		'twenty-twenty-one-ie11-polyfills-asset',
		get_stylesheet_directory_uri() . '/assets/js/polyfills.js',
		array(),
		wp_get_theme()->get( 'Version' ),
		true
	);*/

	// Register the IE11 polyfill loader.
	/*wp_register_script(
		'twenty-twenty-one-ie11-polyfills',
		null,
		array(),
		wp_get_theme()->get( 'Version' ),
		true
	);
	wp_add_inline_script(
		'twenty-twenty-one-ie11-polyfills',
		wp_get_script_polyfill(
			$wp_scripts,
			array(
				'Element.prototype.matches && Element.prototype.closest && window.NodeList && NodeList.prototype.forEach' => 'twenty-twenty-one-ie11-polyfills-asset',
			)
		)
	);*/

	// Main navigation scripts.
	if (has_nav_menu('primary')) {
		wp_enqueue_script(
			'twenty-twenty-one-primary-navigation-script',
			get_stylesheet_directory_uri() . '/assets/js/primary-navigation.js',
			//array( 'twenty-twenty-one-ie11-polyfills' ),
			array(),
			wp_get_theme()->get('Version'),
			true
		);
	}

	// Responsive embeds script.
	wp_enqueue_script(
		'twenty-twenty-one-responsive-embeds-script',
		get_stylesheet_directory_uri() . '/assets/js/responsive-embeds.js',
		//array( 'twenty-twenty-one-ie11-polyfills' ),
		array(),
		wp_get_theme()->get('Version'),
		true
	);

	wp_enqueue_script(
		'script',
		get_theme_file_uri('/assets/js/script.js'),
		array(),
		false,
		true
	);

	wp_enqueue_script(
		'modale',
		get_theme_file_uri('/assets/js/modale.js'),
		array(),
		false,
		true
	);
}
add_action('wp_enqueue_scripts', 'ihag_scripts');

/**
 * Enqueue block editor script.
 *
 * @since Twenty Twenty-One 1.0
 *
 * @return void
 */
function ihag_block_editor_script()
{

	wp_enqueue_script('twentytwentyone-editor', get_theme_file_uri('/assets/js/editor.js'), array('wp-blocks', 'wp-dom'), wp_get_theme()->get('Version'), true);
}

add_action('enqueue_block_editor_assets', 'ihag_block_editor_script');

/**
 * Fix skip link focus in IE11.
 *
 * This does not enqueue the script because it is tiny and because it is only for IE11,
 * thus it does not warrant having an entire dedicated blocking script being loaded.
 *
 * @since Twenty Twenty-One 1.0
 *
 * @link https://git.io/vWdr2
 */
function ihag_skip_link_focus_fix()
{

	// If SCRIPT_DEBUG is defined and true, print the unminified file.
	if (defined('SCRIPT_DEBUG') && SCRIPT_DEBUG) {
		echo '<script>';
		include get_theme_file_path() . '/assets/js/skip-link-focus-fix.js';
		echo '</script>';
	}

	// The following is minified via `npx terser --compress --mangle -- assets/js/skip-link-focus-fix.js`.
?>
	<script>
		/(trident|msie)/i.test(navigator.userAgent) && document.getElementById && window.addEventListener && window.addEventListener("hashchange", (function() {
			var t, e = location.hash.substring(1);
			/^[A-z0-9_-]+$/.test(e) && (t = document.getElementById(e)) && (/^(?:a|select|input|button|textarea)$/i.test(t.tagName) || (t.tabIndex = -1), t.focus())
		}), !1);
	</script>
<?php
}
add_action('wp_print_footer_scripts', 'ihag_skip_link_focus_fix');

/**
 * Enqueue non-latin language styles.
 *
 * @since Twenty Twenty-One 1.0
 *
 * @return void
 */
function ihag_non_latin_languages()
{
	$custom_css = ihag_get_non_latin_css('front-end');

	if ($custom_css) {
		wp_add_inline_style('twenty-twenty-one-style', $custom_css);
	}
}
//add_action( 'wp_enqueue_scripts', 'ihag_non_latin_languages' );

// SVG Icons class.
require get_theme_file_path() . '/classes/class-twenty-twenty-one-svg-icons.php';

// Custom color classes.
//require get_theme_file_path() . '/classes/class-twenty-twenty-one-custom-colors.php';
//new ihag_Custom_Colors();

// Enhance the theme by hooking into WordPress.
require get_theme_file_path() . '/inc/template-functions.php';

// Menu functions and filters.
require get_theme_file_path() . '/inc/menu-functions.php';

// Custom template tags for the theme.
require get_theme_file_path() . '/inc/template-tags.php';

// Customizer additions.
require get_theme_file_path() . '/classes/class-twenty-twenty-one-customize.php';
new ihag_Customize();

// Block Patterns.
require get_theme_file_path() . '/inc/block-patterns.php';

// Block Styles.
require get_theme_file_path() . '/inc/block-styles.php';

require get_theme_file_path() . '/inc/acf-block.php';
require get_theme_file_path() . '/inc/custom-post-type.php';
/**
 * Enqueue scripts for the customizer preview.
 *
 * @since Twenty Twenty-One 1.0
 *
 * @return void
 */
function ihag_customize_preview_init()
{
	wp_enqueue_script(
		'twentytwentyone-customize-helpers',
		get_theme_file_uri('/assets/js/customize-helpers.js'),
		array(),
		wp_get_theme()->get('Version'),
		true
	);
	

	wp_enqueue_script(
		'twentytwentyone-customize-preview',
		get_theme_file_uri('/assets/js/customize-preview.js'),
		array('customize-preview', 'customize-selective-refresh', 'jquery', 'twentytwentyone-customize-helpers'),
		wp_get_theme()->get('Version'),
		true
	);
}
//add_action( 'customize_preview_init', 'ihag_customize_preview_init' );

/**
 * Enqueue scripts for the customizer.
 *
 * @since Twenty Twenty-One 1.0
 *
 * @return void
 */
function ihag_customize_controls_enqueue_scripts()
{

	wp_enqueue_script(
		'twentytwentyone-customize-helpers',
		get_theme_file_uri('/assets/js/customize-helpers.js'),
		array(),
		wp_get_theme()->get('Version'),
		true
	);
}
add_action('customize_controls_enqueue_scripts', 'ihag_customize_controls_enqueue_scripts');

// Custom Button Style ADD

register_block_style(
	'core/button',
	array(
		'name'  => 'bouton-arrow',
		'label' => __('Bouton borderless', 'wp-rig'),
	)
);


// Remove Span & p & br from CF7
add_filter('wpcf7_autop_or_not', '__return_false');
add_filter('wpcf7_form_elements', function ($content) {
	$content = preg_replace('/<(span).*?class="\s*(?:.*\s)?wpcf7-form-control-wrap(?:\s[^"]+)?\s*"[^\>]*>(.*)<\/\1>/i', '\2', $content);
	$content = str_replace('aria-required="true"', 'aria-required="true" required="required" ', $content);
	return $content;
});

//hidden title 
wpcf7_add_shortcode('hidden', 'wpcf7_sourceurl_shortcode_handler', true);

function wpcf7_sourceurl_shortcode_handler($tag) {
    //if (!is_array($tag)) return '';

    $name = $tag['name'];
    if (empty($name)) return '';

    $html = '<input type="hidden" name="' . $name . '" value="' . get_the_title() . '" />';
   return $html;
}



add_filter('get_the_archive_title', function ($title) {
	if (is_category()) {
		$title = single_cat_title('', false);
	} elseif (is_tag()) {
		$title = single_tag_title('', false);
	} elseif (is_author()) {
		$title = '<span class="vcard">' . get_the_author() . '</span>';
	} elseif (is_tax()) { //for custom post types
		$title = sprintf(__('%1$s'), single_term_title('', false));
	} elseif (is_post_type_archive()) {
		$title = post_type_archive_title('', false);
	}
	return $title;
});

add_filter('acf/settings/save_json', 'my_acf_json_save_point');
function my_acf_json_save_point($path)
{
	// update path
	$path = get_stylesheet_directory();
	// return
	return $path;
}

add_filter('acf/settings/load_json', 'my_acf_json_load_point');
function my_acf_json_load_point($paths)
{
	// remove original path (optional)
	unset($paths[0]);
	// append path
	$paths[] = get_stylesheet_directory();
	// return
	return $paths;
}

function my_acf_init()
{

	//acf_update_setting('google_api_key', 'AIzaSyDFKqR7J4PrQ2JurolHm-CO3CuFrCqmLz4');
}
add_action('acf/init', 'my_acf_init');


// recupere le term(valeur) d'une taxonomy (ex: Brad Pitt est le term de la taxonomy actor)
function ihag_get_term($post, $taxonomy)
{
	if (class_exists('WPSEO_Primary_Term')) :
		$wpseo_primary_term = new WPSEO_Primary_Term($taxonomy, get_the_id($post));
		$wpseo_primary_term = $wpseo_primary_term->get_primary_term();
		$term = get_term($wpseo_primary_term);
		if (is_wp_error($term)) {
			$term = get_the_terms($post, $taxonomy);
			return $term[0];
		}
		return $term;
	else :
		$term = get_the_terms($post, $taxonomy);
		return $term[0];
	endif;
}

//Video parameters

add_filter('oembed_fetch_url','example_add_vimeo_args',10,3);
function example_add_vimeo_args($provider, $url, $args) {
    
        $args = array(
            'title' => 0,
            'byline' => 0,
            'portrait' => 0,
            'badge' => 0
        );
        $provider = add_query_arg( $args, $provider );
    
    return $provider;   
}
/*
add_action( 'user_register', 'ihag_synchro_mailjet',10);
add_action( 'deleted_user', 'ihag_synchro_mailjet',10 );
function ihag_synchro_mailjet(){
  $dir_logs = dirname(FILE).'/_logs/';
  if ( !file_exists($dir_logs) ){ mkdir($dir_logs, 0777);}

	$listId = 9341;
	$mj = new \Mailjet\Client('45a0b80182f9605bf7dd6da3b62546b9', 'bb5d867cc2e7dd8137b1d90a10f4bd02');
	$filters = [
		'ContactsList' => $listId
	];
	$response = $mj->get(Resources::$Contact, ['filters' => $filters]);
	$listContactMailjet = $response->getData();
	
	$wpusers = get_users(array( 'fields' => array( 'user_email' ) ) );
	$wp_user_add = [];
	//recherche si le wpUser sont présents mailJet, si ok : on dépile userMailJet,  sinon on ajoute
	foreach ( $wpusers as $user ) {
		$wpUserFindInMailJet = false;
		foreach($listContactMailjet as $key => $contactMailJet){
			if($user->user_email == $contactMailJet['Email']){
				unset($listContactMailjet[$key]);
				$wpUserFindInMailJet = true;
				break;
			}
		}
		if(!$wpUserFindInMailJet){ 
			$wp_user_add[] = [
				'Email' => $user->user_email,
				'IsExcludedFromCampaigns' => "false",
				'Name' => $user->user_email,
				'Properties' => "object"
				];
			
		}
	}
	$body = [
		'Action' => "addnoforce",
		'Contacts' => $wp_user_add
	];
	$response = $mj->post(Resources::$ContactslistManagemanycontacts, ['id' => $listId, 'body' => $body]);

  
	$contactsMailJet = [];
	foreach($listContactMailjet as $key => $contactMailJet){
		$contactsMailJet[] = [
			'Email' => $contactMailJet['Email'],
		];
	}
	$body = [
		'Contacts' => $contactsMailJet,
		'ContactsLists' => [
			[
				'Action' => "remove",
				'ListID' => $listId
			]
		]
	];
	$response = $mj->post(Resources::$ContactManagemanycontacts, ['body' => $body]);

}*/

function ihag_the_custom_logo(){
	$custom_logo_attr = array(
		'class'   => 'custom-logo',
		'loading' => false,
	);
	$unlink_homepage_logo = (bool) get_theme_support( 'custom-logo', 'unlink-homepage-logo' );
	if ( $unlink_homepage_logo && is_front_page() && ! is_paged() ) {
		/*
		 * If on the home page, set the logo alt attribute to an empty string,
		 * as the image is decorative and doesn't need its purpose to be described.
		 */
		$custom_logo_attr['alt'] = '';
	} else {
		/*
		 * If the logo alt attribute is empty, get the site title and explicitly pass it
		 * to the attributes used by wp_get_attachment_image().
		 */
		$image_alt = get_post_meta( $custom_logo_id, '_wp_attachment_image_alt', true );
		if ( empty( $image_alt ) ) {
			$custom_logo_attr['alt'] = get_bloginfo( 'name', 'display' );
		}
		}
	$custom_logo_id = get_theme_mod( 'custom_logo' );
	$custom_logo_attr = apply_filters( 'get_custom_logo_image_attributes', $custom_logo_attr, $custom_logo_id, $blog_id );

	/*
	 * If the alt attribute is not empty, there's no need to explicitly pass it
	 * because wp_get_attachment_image() already adds the alt attribute.
	 */
	$image = wp_get_attachment_image( $custom_logo_id, 'full', false, $custom_logo_attr );
	echo $image;
}