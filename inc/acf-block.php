<?php
function my_plugin_block_categories( $categories, $post ) {
    return array_merge(
        $categories,
        array(
            array(
                'slug' => 'custom',
                'title' => __( 'Custom', 'ihag' ),
                'icon'  => 'star-empty'
            ),
        )
    );
}
add_filter( 'block_categories', 'my_plugin_block_categories', 10, 2 );


add_action('acf/init', 'acf_init_blocs');
function acf_init_blocs() {
    if( function_exists('acf_register_block_type') ) {

        // Basic Blocks

        acf_register_block_type(
            array(
                'name'				    => 'cards',
                'title'				    => __('Carte accueil'),
                'render_template'	    => 'template-parts/block/card.php',
                'category'			    => 'custom',
                //'mode'                  => 'preview',
                'icon'				    => 'admin-page',
                'keywords'			    => array('carte', 'card', 'bloc', 'caractéristiques', 'greenmetrics'),
                'supports'	            => array(
                                            'align'	=> false,
                                            'mode'  => true,
                                        ),
            )
        );

        acf_register_block_type(
            array(
                'name'				    => 'company',
                'title'				    => __('Qui sommes nous ?'),
                'render_template'	    => 'template-parts/block/company.php',
                'category'			    => 'custom',
                'mode'                  => 'preview',
                'icon'				    => 'admin-home',
                'keywords'			    => array('qui', 'sommes', 'greenmetrics'),
                'supports'	            => array(
                                            'align'	=> false,
                                            'mode'  => false,
                                        ),
            )
        );

        acf_register_block_type(
            array(
                'name'				    => 'company_en',
                'title'				    => __('Who are we ?'),
                'render_template'	    => 'template-parts/block/companyen.php',
                'category'			    => 'custom',
                'mode'                  => 'preview',
                'icon'				    => 'admin-home',
                'keywords'			    => array('who', 'we', 'greenmetrics'),
                'supports'	            => array(
                                            'align'	=> false,
                                            'mode'  => false,
                                        ),
            )
        );

        acf_register_block_type(
            array(
                'name'				    => 'video_button',
                'title'				    => __('Bouton vidéo'),
                'render_template'	    => 'template-parts/block/btn_video.php',
                'category'			    => 'custom',
                'mode'                  => 'edit',
                'icon'				    => 'video-alt3',
                'keywords'			    => array('video', 'youtube', 'greenmetrics'),
                'supports'	            => array(
                                            'align'	=> false,
                                            'mode'  => true,
                                        ),
            )
        );

        acf_register_block_type(
            array(
                'name'				    => 'team',
                'title'				    => __('Equipe'),
                'render_template'	    => 'template-parts/block/team.php',
                'category'			    => 'custom',
                'mode'                  => 'preview',
                'icon'				    => 'groups',
                'keywords'			    => array('team', 'equipe', 'greenmetrics'),
                'supports'	            => array(
                                            'align'	=> false,
                                            'mode'  => false,
                                        ),
            )
        );

        acf_register_block_type(
            array(
                'name'				    => 'team_en',
                'title'				    => __('Team en'),
                'render_template'	    => 'template-parts/block/teamen.php',
                'category'			    => 'custom',
                'mode'                  => 'preview',
                'icon'				    => 'groups',
                'keywords'			    => array('team', 'us', 'greenmetrics'),
                'supports'	            => array(
                                            'align'	=> false,
                                            'mode'  => false,
                                        ),
            )
        );

        acf_register_block_type(
            array(
                'name'				    => 'last-posts',
                'title'				    => __('Derniers articles'),
                'render_template'	    => 'template-parts/block/last-posts.php',
                'category'			    => 'custom',
                'mode'                  => 'preview',
                'icon'				    => 'admin-post',
                'keywords'			    => array('dernier', 'article', 'blog'),
                'supports'	            => array(
                                            'align'	=> false,
                                            'mode'  => false,
                                        ),
            )
        );

    }
}