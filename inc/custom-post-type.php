<?php
/* joints Custom Post Type Example
This page walks you through creating
a custom post type and taxonomies. You
can edit this one or copy the following code
to create another one.

I put this in a separate file so as to
keep it organized. I find it easier to edit
and change things if they are concentrated
in their own file.

*/

// adding the function to the Wordpress init
add_action( 'init', 'custom_post_forum');
// let's create the function for the custom type
function custom_post_forum() {
	

	register_post_type( 'job', /* (http://codex.wordpress.org/Function_Reference/register_post_type) */
	 	// let's now add all the options for this post type
		array('labels' 			=> array(
				'name' 				=> __('recrutements', 'greenmetrics'), /* This is the Title of the Group */
				'singular_name' 	=> __('recrutement', 'greenmetrics'), /* This is the individual type */
			), /* end of arrays */
			'menu_position' 	=> 18, /* this is what order you want it to appear in on the left hand side menu */
			'menu_icon' 		=> 'dashicons-businessperson', /* the icon for the custom post type menu. uses built-in dashicons (CSS class name) */
			'hierarchical' 		=> false,
			'public'             => true,
			'publicly_queryable' => true,
			'show_ui'            => true,
			'show_in_menu'       => true,
			'query_var'          => true,
			'show_in_rest'		=> true,
			'supports' 			=> array( 'title', 'editor')
	 	) /* end of options */
	);

	
}
