<?php
/*
Template Name: Job
*/
get_header();

/* Start the Loop */
while ( have_posts() ) :
	the_post();
	get_template_part( 'template-parts/content/content-page' );
	?>
<div class="wp-block-cover alignfull has-transparent-background-color has-background-dim">
	<div class="wp-block-cover__inner-container">
	<div class="wp-block-columns alignwide recrutement" style="margin-bottom:4rem;">
	<div class="wp-block-column">
		<p class="has-green-color has-text-color" style="font-size:3rem">
			<i class="wp-svg-custom-Icon-ionic-ios-settings Icon-ionic-ios-settings"></i>
		</p>
		<h2><strong><?php esc_html_e('Our offers','twentytwentyone');?></strong></h2>
		<p><?php esc_html_e('We’re always looking for new talent! Don’t hesitate to send us your application!  ','twentytwentyone');?></p>
		<form method="POST" action="<?php the_permalink();?>#job-result">
			<input type="text" id="job-search" name="job_search" placeholder="<?php _e("Find an offer", "twentytwentyone");?>">
			<input id="validate-job" type="submit" value="✔">
		</form>
	</div>
	<div class="wp-block-column mobile-unshown">
		<div class="wp-block-image">
			<figure class="">
				<img class="" src="/wp-content/themes/greenmetrics/assets/images/illu2.png" alt="" style='width:350px; margin: 0 auto;'>
			</figure>
		</div>
		
	</div>
</div>
<div class="offer-grid alignwide">
	<?php

	$tab_posts = array();
	global $post;
	$args = array( 	'posts_per_page' => -1,
					'post_type' => 'job'
				);
	
	
	if(isset($_POST['job_search']) && !empty($_POST['job_search']) ){
		$args['s'] = $_POST['job_search'];
	}
	$myposts = get_posts( $args );
	foreach ( $myposts as $post ) : setup_postdata( $post );
	?>
	<div class="offer" id="job-result">
		<p class="has-green-color has-text-color" style="font-size:1.5rem">
			<i class="wp-svg-custom-Icon-ionic-ios-settings Icon-ionic-ios-settings"></i>
		</p>
		<a href="<?php the_permalink();?>" style="text-decoration:none;">
			<h3>
				<?php
				the_title();
				?>
				<br>
				<span>
					<?php
					the_field("level");
					?>
				</span>
			</h3>			
		</a>
		<a class="btn-article" href="<?php the_permalink();?>"><?php _e("Check the offer", "twentytwentyone");?></a>
	</div>
	<?php
		
	endforeach;
	wp_reset_postdata();
	
	endwhile; // End of the loop.
	?>


	<div class="spont offer" id="job-result">
		<h3>
			Aucune offre ne correspond à ton profil ?
		</h3>	
		<p>
			Nous sommes toujours à la recherche de nouveaux talents. N’hésitez pas à postuler !
		</p>		
		<button class="btn-spont" id="btn-modale-job"> <p><?php _e("Candidature spontanée", "twentytwentyone");?></p> </button>
	</div>
</div>
</div>
</div>

<div id="modal-job" class="modal-job">
        <div class="modal-box">
            <img id="btn-modal-job-close">
            <h2>Candidature spontanée</h2>
        <?php 
        echo do_shortcode(get_field("short_code")); 
        ?>
        </div>
        
    </div>
<?php
get_footer();
?>