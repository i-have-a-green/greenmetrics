<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package WordPress
 * @subpackage Twenty_Twenty_One
 * @since Twenty Twenty-One 1.0
 */

get_header();

/* Start the Loop */
while ( have_posts() ) :
	the_post();
	$args = array(
		'post_type' => 'page',
		'posts_per_page' => 1,
		'meta_query' => array(
			array(
				'key' => '_wp_page_template',
				'value' => 'page-job.php'
			)
		)
	);
	$the_pages = new WP_Query( $args );
	?>
	<div id="modal-job" class="modal-job">
		<div class="modal-box">
			<img id="btn-modal-job-close">
			<h2>Candidature <br> <span><?php the_title() ?></span></h2>
		<?php 
		$permalink_page_job = '';
		if( $the_pages->have_posts() ){
			while( $the_pages->have_posts() ){
				$the_pages->the_post();
				echo do_shortcode(get_field("short_code"));	
				$permalink_page_job = get_permalink($post->ID);
			}
		}
		wp_reset_postdata();
		?>
		</div>
		
	</div><!-- .entry-content -->

	<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

	<header class="entry-header alignwide" style="padding-bottom:unset;">
		<a href="<?php echo $permalink_page_job;?>#job-result">
			<p class="meta-nav">
				<?php 
					$twentytwentyone_prev =is_rtl() ? ihag_get_icon_svg( 'ui', 'arrow_right' ) : ihag_get_icon_svg( 'ui', 'arrow_left' );
					_e($twentytwentyone_prev . "Retour aux jobs" , "greenmetrics");
				?>					
			</p>
		</a>

	<div class="default-max-width header-job">
		<h1>
			<?php the_title();?>
		</h1>
		<h2>
			<?php the_field("sub-title");?>
			<br>
			<span>
				<?php 
					_e("Publié le ", "greenmetrics");
					echo get_the_date();
				?>
			</span>
		</h2>
	</div>
		
	</header><!-- .entry-header -->

	<div class="entry-content alignwide">
		<?php
		the_content();
		?>
		<button id="btn-modale-job" class="btn-article">
			<?php _e("Postuler à cette offre", "greenmetrics");?>
		</button>
		

	</article><!-- #post-<?php the_ID(); ?> -->
	<?php

endwhile; // End of the loop.

get_footer();
