<?php
/**
 * Block Name: video-button
 */
 ?>
<?php $block_uniq_id = "id_".uniqid(); ?>
<div class="wp-block-button is-style-bouton-arrow">
    <a class="wp-block-button__link has-white-color has-text-color btn-modale" data-uniq-id="<?php echo $block_uniq_id;?>" href="#" >&gt; <?php the_field("label");?> </a>
</div>
<script> 
    if (typeof iframe === 'undefined') {
        var iframe = new Object();
    }
    iframe.<?php echo $block_uniq_id;?> = '<?php 
    $value = get_field('link');
    $value = preg_replace('/src="(.+?)"/', 'src="$1&autoplay=1&controls=2"', $value);
    echo $value;
    ?>'; 
</script>