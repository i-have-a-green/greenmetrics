<?php
/**
 * Block Name: Company
 */
 ?>

<!-- <div>Qui sommes-nous ?</div> -->

<div class="wrapper-company alignwide">

    <div class="alignwide wp-block-columns col-50-50">
        <div class="wp-block-column">
            <p class="has-green-color has-text-color" style="font-size:2.25rem">
                <i class="wp-svg-custom-Groupe-8783 Groupe-8783"></i>
            </p>
            <h2 class='has-gray-222-color'>Le climat, une révolution à mener dès aujourd'hui</h2>
            <p class="intro">Chez Greenmetrics, nous rendons visible l’invisible en mesurant et analysant la date de vos usages et équipements. Notre objectif : aider votre entreprise à devenir actrice du changement !</p>
        </div>
        <div class="wp-block-column">
            <div class="card-company txt-center">
                <p style="color:#FF5964">
                    <i class="wp-svg-custom-Groupe-7674 Groupe-7674"></i>
                </p>
                <h3>15 millions de tonnes de CO²</h3>
                <p>C’est la quantité d’émissions du secteur du numérique en France en 2019.</p>
                <a target="_blank" href="http://www.senat.fr/rap/a20-233/a20-2331.html">Source</a>
            </div>
        </div>
    </div>

    <div class="alignwide wp-block-columns col-50-50">
        <div class="wp-block-column txt-center">
            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 45 45" style="enable-background:new 0 0 45 45" xml:space="preserve"><g transform="rotate(90 22.5 22.5)"><circle cx="22.5" cy="22.5" r="22.5" style="fill:transparent"/><path class="st1" d="M45 22.5C45 34.9 34.9 45 22.5 45S0 34.9 0 22.5 10.1 0 22.5 0 45 10.1 45 22.5zm-42 0C3 33.3 11.7 42 22.5 42S42 33.3 42 22.5 33.3 3 22.5 3 3 11.7 3 22.5z"/></g><path class="st1" d="M31.2 18.3 22.5 29l-8.7-10.7h17.4z"/><path class="st1" d="M22.5 30.5c-.4 0-.9-.2-1.2-.5l-8.7-10.7c-.4-.4-.4-1.1-.2-1.6s.8-.9 1.4-.9h17.5c.6 0 1.1.3 1.4.9s.2 1.1-.2 1.6L23.8 30c-.4.3-.9.5-1.3.5zm-5.6-10.7 5.6 6.8 5.6-6.8H16.9z"/></svg>
            <h3>Le numérique, source de pollution</h3>
            <p>Accélérateurs de croissance et d’attractivité pour les entreprises, les outils numériques améliorent aussi la communication et le partage d’information. Cependant, ils contribuent largement au changement climatique. A chaque étape de leur cycle de vie, ils sont à l’origine d’émissions de CO2 et ont ainsi, pour la plupart, un impact carbone négatif.</p>
        </div>
        <div class="wp-block-column">
            <div class="card-company txt-center">
                <p style="color:#5B3AFF">
                    <i class="wp-svg-custom-Groupe-7674 Groupe-7674"></i>
                </p>
                <h3>4,2% des énergies primaires</h3>
                <p>C’est la consommation totale du secteur du numérique dans le monde.</p>
                <a target="_blank" href="http://www.senat.fr/rap/r19-555/r19-555_mono.html">Source</a>
            </div>
        </div>
    </div>

    <div class="alignwide wp-block-columns col-50-50">
        <div class="wp-block-column txt-center">
            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 45 45" style="enable-background:new 0 0 45 45" xml:space="preserve"><g transform="rotate(90 22.5 22.5)"><circle cx="22.5" cy="22.5" r="22.5" style="fill:transparent"/><path class="st1" d="M45 22.5C45 34.9 34.9 45 22.5 45S0 34.9 0 22.5 10.1 0 22.5 0 45 10.1 45 22.5zm-42 0C3 33.3 11.7 42 22.5 42S42 33.3 42 22.5 33.3 3 22.5 3 3 11.7 3 22.5z"/></g><path class="st1" d="M31.2 18.3 22.5 29l-8.7-10.7h17.4z"/><path class="st1" d="M22.5 30.5c-.4 0-.9-.2-1.2-.5l-8.7-10.7c-.4-.4-.4-1.1-.2-1.6s.8-.9 1.4-.9h17.5c.6 0 1.1.3 1.4.9s.2 1.1-.2 1.6L23.8 30c-.4.3-.9.5-1.3.5zm-5.6-10.7 5.6 6.8 5.6-6.8H16.9z"/></svg>
            <h3>Vers l’épuisement des ressources naturelles</h3>
            <p>En France, le jour du dépassement a eu lieu le 7 mai 2021. L’état de santé de notre planète se dégrade et les ressources naturelles s’amenuisent d’année en année. Les équipements numériques, très gros consommateurs d’énergies fossiles et de matières premières, tendent à aggraver le problème au fil des évolutions technologiques.</p>
        </div>
        <div class="wp-block-column">
            <div class="card-company txt-center">
                <p style="color:#FE9500">
                    <i class="wp-svg-custom-Groupe-7674 Groupe-7674"></i>
                </p>
                <h3>1 milliard d’euros par an</h3>
                <p>C’est le coût de la pollution numérique en France.</p>
                <a target="_blank" href="https://www.lesechos.fr/tech-medias/hightech/impact-environnemental-du-numerique-le-senat-tire-la-sonnette-dalarme-1218106">Source</a>
            </div>
        </div>
    </div>

    <div class="alignwide wp-block-columns col-50-50">
        <div class="wp-block-column txt-center">
            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 45 45" style="enable-background:new 0 0 45 45" xml:space="preserve"><g transform="rotate(90 22.5 22.5)"><circle cx="22.5" cy="22.5" r="22.5" style="fill:transparent"/><path class="st1" d="M45 22.5C45 34.9 34.9 45 22.5 45S0 34.9 0 22.5 10.1 0 22.5 0 45 10.1 45 22.5zm-42 0C3 33.3 11.7 42 22.5 42S42 33.3 42 22.5 33.3 3 22.5 3 3 11.7 3 22.5z"/></g><path class="st1" d="M31.2 18.3 22.5 29l-8.7-10.7h17.4z"/><path class="st1" d="M22.5 30.5c-.4 0-.9-.2-1.2-.5l-8.7-10.7c-.4-.4-.4-1.1-.2-1.6s.8-.9 1.4-.9h17.5c.6 0 1.1.3 1.4.9s.2 1.1-.2 1.6L23.8 30c-.4.3-.9.5-1.3.5zm-5.6-10.7 5.6 6.8 5.6-6.8H16.9z"/></svg>
            <h3>Un impact économique</h3>
            <p>En plus de coûter cher à l’environnement, la pollution numérique représente un coût économique certain à l’échelle mondiale. C’est aussi une source de dépenses importantes pour les entreprises, notamment en énergies, en équipements et en traitement des déchets.</p>
        </div>
        <div class="wp-block-column">
            <div class="card-company txt-center">
                <p style="color:#0AF1D0">
                    <i class="wp-svg-custom-Groupe-7674 Groupe-7674"></i>
                </p>
                <h3>80% des clients sont sensibles au combat</h3>
                <p>Mené contre la pollution numérique et en faveur du climat.</p>
                <a target="_blank" href="https://www.novethic.fr/actualite/environnement/climat/isr-rse/climat-les-entreprises-europeennes-peuvent-mieux-faire-sur-le-reporting-146652.html">Source</a>
            </div>
        </div>
    </div>

    <div class="alignwide wp-block-columns">
        <div class="wp-block-column txt-center">
            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 45 45" style="enable-background:new 0 0 45 45" xml:space="preserve"><g transform="rotate(90 22.5 22.5)"><circle cx="22.5" cy="22.5" r="22.5" style="fill:transparent"/><path class="st1" d="M45 22.5C45 34.9 34.9 45 22.5 45S0 34.9 0 22.5 10.1 0 22.5 0 45 10.1 45 22.5zm-42 0C3 33.3 11.7 42 22.5 42S42 33.3 42 22.5 33.3 3 22.5 3 3 11.7 3 22.5z"/></g><path class="st1" d="M31.2 18.3 22.5 29l-8.7-10.7h17.4z"/><path class="st1" d="M22.5 30.5c-.4 0-.9-.2-1.2-.5l-8.7-10.7c-.4-.4-.4-1.1-.2-1.6s.8-.9 1.4-.9h17.5c.6 0 1.1.3 1.4.9s.2 1.1-.2 1.6L23.8 30c-.4.3-.9.5-1.3.5zm-5.6-10.7 5.6 6.8 5.6-6.8H16.9z"/></svg>
            <h3>Un nouvel enjeu pour les entreprises</h3>
            <p>En réduisant les dépenses liées à la pollution numérique, celles-ci pourront améliorer leur image et leur marque employeur. À court terme, il s’agit aussi d’assurer la conformité de l’entreprise avec le cadre légal, de plus en plus précis et complet.</p>
        </div>
        <div class="wp-block-column" style="flex-basis:16px"></div>
        <div class="wp-block-column img">
            <img src="/wp-content/themes/greenmetrics/assets/images/illu1.png" alt="" class="mobile-unshown">
        </div>
    </div>

</div>