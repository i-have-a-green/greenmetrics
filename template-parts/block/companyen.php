<?php
/**
 * Block Name: Company EN
 */
 ?>

<!-- <div>Qui sommes-nous ?</div> -->

<div class="wrapper-company alignwide">

    <div class="alignwide wp-block-columns col-50-50">
        <div class="wp-block-column">
            <p class="has-green-color has-text-color" style="font-size:2.25rem">
                <i class="wp-svg-custom-Groupe-8783 Groupe-8783"></i>
            </p>
            <h2 class='has-gray-222-color'>Climate, the revolution is already underway</h2>
            <p class="intro">At Greenmetrics, we make the invisible visible by measuring and analyzing data from your practices and equipment. Our goal: to help your company become an actor of change!</p>
        </div>
        <div class="wp-block-column">
            <div class="card-company txt-center">
                <p style="color:#FF5964">
                    <i class="wp-svg-custom-Groupe-7674 Groupe-7674"></i>
                </p>
                <h3>15 million tons of CO2</h3>
                <p>That's the amount of emissions coming from the digital industry in France in 2019.</p>
                <a href="http://">Source</a>
            </div>
        </div>
    </div>

    <div class="alignwide wp-block-columns col-50-50">
        <div class="wp-block-column txt-center">
            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 45 45" style="enable-background:new 0 0 45 45" xml:space="preserve"><g transform="rotate(90 22.5 22.5)"><circle cx="22.5" cy="22.5" r="22.5" style="fill:transparent"/><path class="st1" d="M45 22.5C45 34.9 34.9 45 22.5 45S0 34.9 0 22.5 10.1 0 22.5 0 45 10.1 45 22.5zm-42 0C3 33.3 11.7 42 22.5 42S42 33.3 42 22.5 33.3 3 22.5 3 3 11.7 3 22.5z"/></g><path class="st1" d="M31.2 18.3 22.5 29l-8.7-10.7h17.4z"/><path class="st1" d="M22.5 30.5c-.4 0-.9-.2-1.2-.5l-8.7-10.7c-.4-.4-.4-1.1-.2-1.6s.8-.9 1.4-.9h17.5c.6 0 1.1.3 1.4.9s.2 1.1-.2 1.6L23.8 30c-.4.3-.9.5-1.3.5zm-5.6-10.7 5.6 6.8 5.6-6.8H16.9z"/></svg>
            <h3>Digital technology, a source of pollution </h3>
            <p>Digital tools accelerate growth and attractiveness for companies and improve communication and data sharing. At the same time, they are also contributing significantly to climate change. At each stage of their life cycle, they cause CO2 emissions and therefore, in most cases, have a negative carbon impact. </p>
        </div>
        <div class="wp-block-column">
            <div class="card-company txt-center">
                <p style="color:#5B3AFF">
                    <i class="wp-svg-custom-Groupe-7674 Groupe-7674"></i>
                </p>
                <h3>4.2% of primary energy</h3>
                <p>That's how much energy the digital sector consumes worldwide.</p>
                <a href="http://">Source</a>
            </div>
        </div>
    </div>

    <div class="alignwide wp-block-columns col-50-50">
        <div class="wp-block-column txt-center">
            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 45 45" style="enable-background:new 0 0 45 45" xml:space="preserve"><g transform="rotate(90 22.5 22.5)"><circle cx="22.5" cy="22.5" r="22.5" style="fill:transparent"/><path class="st1" d="M45 22.5C45 34.9 34.9 45 22.5 45S0 34.9 0 22.5 10.1 0 22.5 0 45 10.1 45 22.5zm-42 0C3 33.3 11.7 42 22.5 42S42 33.3 42 22.5 33.3 3 22.5 3 3 11.7 3 22.5z"/></g><path class="st1" d="M31.2 18.3 22.5 29l-8.7-10.7h17.4z"/><path class="st1" d="M22.5 30.5c-.4 0-.9-.2-1.2-.5l-8.7-10.7c-.4-.4-.4-1.1-.2-1.6s.8-.9 1.4-.9h17.5c.6 0 1.1.3 1.4.9s.2 1.1-.2 1.6L23.8 30c-.4.3-.9.5-1.3.5zm-5.6-10.7 5.6 6.8 5.6-6.8H16.9z"/></svg>
            <h3>On the road to depleting natural resources </h3>
            <p>In France, Earth Overshoot day took place on May 7, 2021. Our planet's health is deteriorating and natural resources are dwindling year after year. Digital equipment, which is a major consumer of fossil fuels and raw materials, is contributing to the problem as technology evolves. </p>
        </div>
        <div class="wp-block-column">
            <div class="card-company txt-center">
                <p style="color:#FE9500">
                    <i class="wp-svg-custom-Groupe-7674 Groupe-7674"></i>
                </p>
                <h3>1 billion euros per year</h3>
                <p>The cost of digital pollution in France.</p>
                <a href="http://">Source</a>
            </div>
        </div>
    </div>

    <div class="alignwide wp-block-columns col-50-50">
        <div class="wp-block-column txt-center">
            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 45 45" style="enable-background:new 0 0 45 45" xml:space="preserve"><g transform="rotate(90 22.5 22.5)"><circle cx="22.5" cy="22.5" r="22.5" style="fill:transparent"/><path class="st1" d="M45 22.5C45 34.9 34.9 45 22.5 45S0 34.9 0 22.5 10.1 0 22.5 0 45 10.1 45 22.5zm-42 0C3 33.3 11.7 42 22.5 42S42 33.3 42 22.5 33.3 3 22.5 3 3 11.7 3 22.5z"/></g><path class="st1" d="M31.2 18.3 22.5 29l-8.7-10.7h17.4z"/><path class="st1" d="M22.5 30.5c-.4 0-.9-.2-1.2-.5l-8.7-10.7c-.4-.4-.4-1.1-.2-1.6s.8-.9 1.4-.9h17.5c.6 0 1.1.3 1.4.9s.2 1.1-.2 1.6L23.8 30c-.4.3-.9.5-1.3.5zm-5.6-10.7 5.6 6.8 5.6-6.8H16.9z"/></svg>
            <h3>Economic impact</h3>
            <p>In addition to having a cost on the environment, digital pollution represents a significant economic cost on a global scale. It is also a source of major expenses for companies, especially in terms of energy, equipment and waste treatment. </p>
        </div>
        <div class="wp-block-column">
            <div class="card-company txt-center">
                <p style="color:#0AF1D0">
                    <i class="wp-svg-custom-Groupe-7674 Groupe-7674"></i>
                </p>
                <h3>80% of clients feel concerned about the fight</h3>
                <p>Carried out against digital pollution and in support of the climate.</p>
                <a href="http://">Source</a>
            </div>
        </div>
    </div>

    <div class="alignwide wp-block-columns">
        <div class="wp-block-column txt-center">
            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 45 45" style="enable-background:new 0 0 45 45" xml:space="preserve"><g transform="rotate(90 22.5 22.5)"><circle cx="22.5" cy="22.5" r="22.5" style="fill:transparent"/><path class="st1" d="M45 22.5C45 34.9 34.9 45 22.5 45S0 34.9 0 22.5 10.1 0 22.5 0 45 10.1 45 22.5zm-42 0C3 33.3 11.7 42 22.5 42S42 33.3 42 22.5 33.3 3 22.5 3 3 11.7 3 22.5z"/></g><path class="st1" d="M31.2 18.3 22.5 29l-8.7-10.7h17.4z"/><path class="st1" d="M22.5 30.5c-.4 0-.9-.2-1.2-.5l-8.7-10.7c-.4-.4-.4-1.1-.2-1.6s.8-.9 1.4-.9h17.5c.6 0 1.1.3 1.4.9s.2 1.1-.2 1.6L23.8 30c-.4.3-.9.5-1.3.5zm-5.6-10.7 5.6 6.8 5.6-6.8H16.9z"/></svg>
            <h3>A new challenge for companies</h3>
            <p>By reducing expenses related to digital pollution, they will be able to upgrade their image and employer brand. In the short term, it is also a question of ensuring the company's compliance with legal frameworks, which is becoming increasingly detailed and exhaustive.</p>
        </div>
        <div class="wp-block-column" style="flex-basis:16px"></div>
        <div class="wp-block-column img">
            <img src="/wp-content/themes/greenmetrics/assets/images/illu1.png" alt="" class="mobile-unshown">
        </div>
    </div>
</div>