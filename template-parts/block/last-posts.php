<?php
/**
* Block Name: Bloc last-posts
*/
?>
<div class="diagonal-background alignfull"></div>
<div class="bg-post wave-background wave-up alignfull no-bot-wave">
    <section class="last-posts alignwide">
        <p class="has-green-color has-text-color blog-ico" style="font-size:2.25rem">
            <i class="wp-svg-custom-Icon-ionic-ios-chatbubbles Icon-ionic-ios-chatbubbles"></i>
        </p>
        <div class="wrapper bloc-vertical-spacing">
            <h2 class="section-title center underline"><?php _e("Retrouvez l’actualité de notre entreprise sur notre blog", "greenmetrics");?></h2>
            <div class="articles-list">
                <?php
                    $args = array(
                        'posts_per_page' => 3,
                        'post_status'    => 'publish',
                    );
                    $the_query = new WP_Query( $args );
                    if ( $the_query->have_posts() ) :
                        while ( $the_query->have_posts() ) : $the_query->the_post();
                            get_template_part( 'template-parts/content/content', 'excerpt' );
                        endwhile;
                        wp_reset_postdata();
                    endif; 
                ?>
            </div>
        </div>
    </section>
</div>