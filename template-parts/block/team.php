<?php
/**
 * Block Name: Team
 */
 ?>

<!-- <div>bloc équipe</div> -->

<div class="grid-members alignwide">

    <div class="membre">
        <div class="wp-block-columns">
            <div class="wp-block-column">
                <img src="/wp-content/themes/greenmetrics/assets/images/member/Nicholas.png" alt="Nicholas">
            </div>
            <div class="wp-block-column">
                <h3>
                    Nicholas Mouret <br> <span>Co-fondateur et CEO</span>
                </h3>
                <p>
                    Fondateur et directeur d’une société de recyclage à succès, Nicholas est convaincu que la révolution climatique est une source d’opportunités pour faire évoluer nos comportements. Grâce à Greenmetrics, il donne aux entreprises les clés pour lutter efficacement contre les émissions de CO², en commençant par pollution numérique.
                </p>
            </div>
        </div>
    </div> 

    <div class="membre">
        <div class="wp-block-columns">
            <div class="wp-block-column">
                <img src="/wp-content/themes/greenmetrics/assets/images/member/Baptiste.png" alt="">
            </div>
            <div class="wp-block-column">
                <h3>
                    Baptiste Thomas <br> <span>Co-fondateur et CTO</span>
                </h3>
                <p>
                    Entrepreneur dans l’âme, Baptiste est aussi passionné par le digital et les logiciels. Il fait désormais converger ses compétences techniques et ses convictions écologiques pour aider les entreprises à mieux comprendre les enjeux de la pollution numérique.
                </p>
            </div>
        </div>
    </div> 


    <div class="membre">
        <div class="wp-block-columns">
            <div class="wp-block-column">
                <img src="/wp-content/themes/greenmetrics/assets/images/member/Daniel.png" alt="">
            </div>
            <div class="wp-block-column">
                <h3>
                    Daniel Lahyani <br> <span>Co-fondateur et CDO</span>
                </h3>
                <p>
                    Expert Data et Analytics, Daniel a pour conviction que la valorisation des données est un réel levier de performance pour les entreprises. Son objectif est d’aider les entreprises à remettre le numérique au cœur de leur impact et de leur stratégie bas carbone.
                </p>
            </div>
        </div>
    </div> 

    <div class="membre">
        <div class="wp-block-columns">
            <div class="wp-block-column">
                <img src="/wp-content/themes/greenmetrics/assets/images/member/Jonathan.png" alt="">
            </div>
            <div class="wp-block-column">
                <h3>
                    Jonathan Cyrot <br> <span>Co-fondateur et strategic advisor</span>
                </h3>
                <p>
                    Expert de la finance mais aussi entrepreneur et business angel, Jonathan a toujours été au cœur des problématiques stratégiques des entreprises. Avec Greenmetrics, il participe à transformer la gestion carbone tout en optimisant les dépenses.
                </p>
            </div>
        </div>
    </div> 

    <div class="membre">
        <div class="wp-block-columns">
            <div class="wp-block-column">
                <img src="/wp-content/themes/greenmetrics/assets/images/member/Jordan.png" alt="">
            </div>
            <div class="wp-block-column">
                <h3>
                    Jordan Zeitoun <br> <span>Co-fondateur et CFO</span>
                </h3>
                <p>
                    Entrepreneur et investisseur, Jordan a notamment fondé la marque Yann Couvreur et accompagné les plus grandes entreprises dans leur restructuration financière. À travers Greenmetrics, il aide les entreprises à se développer de manière responsable.
                </p>
            </div>
        </div>
    </div> 

</div>


