<?php
/**
 * Block Name: Team EN
 */
 ?>

<!-- <div>bloc équipe</div> -->

<div class="grid-members alignwide">

    <div class="membre">
        <div class="wp-block-columns">
            <div class="wp-block-column">
                <img src="/wp-content/themes/greenmetrics/assets/images/member/Nicholas.png" alt="Nicholas">
            </div>
            <div class="wp-block-column">
                <h3>
                    Nicholas Mouret <br> <span>Co-founder and CEO</span>
                </h3>
                <p>
                    Founder and CEO of a successful recycling company, Nicholas is convinced that the climate revolution is a source of opportunities to start changing our behavior. Thanks to Greenmetrics, he gives companies the keys to effectively fight against CO2 emissions, starting with digital pollution.
                </p>
            </div>
        </div>
    </div> 

    <div class="membre">
        <div class="wp-block-columns">
            <div class="wp-block-column">
                <img src="/wp-content/themes/greenmetrics/assets/images/member/Baptiste.png" alt="">
            </div>
            <div class="wp-block-column">
                <h3>
                    Baptiste Thomas <br> <span>Co-founder and CTO</span>
                </h3>
                <p>
                    An entrepreneur at heart, Baptiste is also passionate about anything IT-related. He brings together his technical skills and his ecological convictions to help companies better understand the challenges of digital pollution.
                </p>
            </div>
        </div>
    </div> 


    <div class="membre">
        <div class="wp-block-columns">
            <div class="wp-block-column">
                <img src="/wp-content/themes/greenmetrics/assets/images/member/Daniel.png" alt="">
            </div>
            <div class="wp-block-column">
                <h3>
                    Daniel Lahyani <br> <span>Co-founder and CDO</span>
                </h3>
                <p>
                    Daniel is an expert in data and analytics, and believes that leveraging data is a real performance driver for companies. His goal is to help companies place digital data at the core of their impact and low-carbon strategy.
                </p>
            </div>
        </div>
    </div> 

    <div class="membre">
        <div class="wp-block-columns">
            <div class="wp-block-column">
                <img src="/wp-content/themes/greenmetrics/assets/images/member/Jonathan.png" alt="">
            </div>
            <div class="wp-block-column">
                <h3>
                    Jonathan Cyrot <br> <span>Co-founder and strategic advisor</span>
                </h3>
                <p>
                    Jonathan is a financial expert, entrepreneur and business angel. He has always been at the forefront of strategic challenges for companies. With Greenmetrics, he helps transform carbon management and optimize expenses.
                </p>
            </div>
        </div>
    </div> 

    <div class="membre">
        <div class="wp-block-columns">
            <div class="wp-block-column">
                <img src="/wp-content/themes/greenmetrics/assets/images/member/Jordan.png" alt="">
            </div>
            <div class="wp-block-column">
                <h3>
                    Jordan Zeitoun <br> <span>Co-founder and CFO</span>
                </h3>
                <p>
                    Entrepreneur and investor, Jordan founded the trademark Yann Couvreur and accompanied major companies in their path to financial restructuring. Through Greenmetrics, he helps companies grow in a responsible way.
                </p>
            </div>
        </div>
    </div> 

</div>


