<?php
/**
 * Template part for displaying post archives and search results
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package WordPress
 * @subpackage Twenty_Twenty_One
 * @since Twenty Twenty-One 1.0
 */

?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<p>
        <i class="wp-svg-custom-Groupe-7674 Groupe-7674 ico-article" style='font-size:1.5rem'></i>
    </p>
	<?php get_template_part( 'template-parts/header/excerpt-header', get_post_format() ); ?>

	<div class="entry-content">
		<?php get_template_part( 'template-parts/excerpt/excerpt', get_post_format() ); ?>
	</div><!-- .entry-content -->

<!-- 	<footer class="entry-footer default-max-width">
		<?php ihag_entry_meta_footer(); ?>
	</footer> --><!-- .entry-footer -->

	<a href="<?php the_permalink(); ?>" class="btn-article">Lire la suite</a>

</article><!-- #post-${ID} -->
