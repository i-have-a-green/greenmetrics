<?php
/**
 * Template part for displaying posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package WordPress
 * @subpackage Twenty_Twenty_One
 * @since Twenty Twenty-One 1.0
 */

?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

	<header class="entry-header single-header alignwide">
		<div class="wrapper-single ">
			<?php //the_breadcrumb();
				$term = ihag_get_term($post, "category");
			?>
			<a href="<?php echo get_term_link($term);?>">
				<p class="meta-nav">
					<?php 
						$twentytwentyone_prev = is_rtl() ? ihag_get_icon_svg( 'ui', 'arrow_right' ) : ihag_get_icon_svg( 'ui', 'arrow_left' );
						_e($twentytwentyone_prev . "Retour aux articles" , "greenmetrics");
					?>					
				</p>
			</a>
			<ul>
				<?php
					$terms = get_the_terms($post, "category");
					foreach ( $terms as $term ) :?>
					<li style="background-color:<?php the_field('color', $term);?>">
						<a href="<?php echo get_term_link($term);?>">
							<?php echo ''.$term->name . '';?>
						</a> 
					</li>
				<?php endforeach;?>
			</ul>		
		</div>
		<div class="default-max-width">
		<ul id="breadcrumblist" itemscope itemtype="http://schema.org/BreadcrumbList">
			<li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem"><a itemprop="item" href="<?php echo home_url(); ?>"><span itemprop="name"><?php _e('Accueil','greenmetrics');?></span></a>/<meta itemprop="position" content="1" /></li>

			<?php 
				$cat = get_term(get_option("default_category"), "category");
			?>

			<li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem"><a itemprop="item" href="<?php echo get_term_link($cat); ?>"><span itemprop="name"><?php echo $cat->name;?></span></a><meta itemprop="position" content="2" />/</li>
			<?php
				foreach ( $terms as $term ) :?>
				<?php if($term->term_id != $cat->term_id):?>
					<li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
					
						<a itemprop="item" href="<?php echo get_term_link($term);?>">
							<span itemprop="name">
								<?php echo ''.$term->name . '';?>
							</span>
						</a>
						-
						<meta itemprop="position" content="3"/>					
					</li>
				<?php 
				endif;
				endforeach;
			?>
		</ul>
			<p class="article-single">
				<span>
					<?php
					_e("Publié le ", "greenmetrics");
					echo get_the_date();
					?>				
				</span>
				<br>
				<?php
				_e("Par ", "greenmetrics");
				echo get_the_author();
				?>			
			</p>
			<?php the_title( '<h1 class="entry-title" style="font-size:55px;"><strong>', '</strong></h1>' ); ?>
			<?php //ihag_post_thumbnail(); ?>			
		</div>

	</header><!-- .entry-header -->

	<div class="entry-content alignwide">
		<?php
		the_content();

		/*wp_link_pages(
			array(
				'before'   => '<nav class="page-links" aria-label="' . esc_attr__( 'Page', 'twentytwentyone' ) . '">',
				'after'    => '</nav>',
				translators: %: Page number. 
				'pagelink' => esc_html__( 'Page %', 'twentytwentyone' ),
			)
		);*/
		?>
	</div><!-- .entry-content -->

	


</article><!-- #post-<?php the_ID(); ?> -->
